package src;

import src.design.destination.Activity;
import src.design.destination.Destination;
import src.design.passenger.GoldPassenger;
import src.design.passenger.Passenger;
import src.design.passenger.PremiumPassenger;
import src.design.passenger.StandardPassenger;

import java.util.Set;

public class TravelPackageTestMain {

    public static void main(String[] args) {

        TravelPackage goa = new TravelPackage("Goa");

        Activity waterSport = new Activity("Water Sport", "Get ready for the fun and adventure that awaits you at Baga Beach", 5000.);
        Activity paragliding = new Activity("Paragliding", "Get ready for the fun and adventure that awaits you at Baga Beach", 1000.);
        Activity treaking = new Activity("Treaking", "Get ready for the fun and adventure that awaits you at Baga Beach", 2000.);

      //  Set<Activity> activities = Set.of(waterSport, paragliding, treaking);

        Destination anjunaBeach = new Destination("Anjuna Beach");

        anjunaBeach.addActivity(waterSport);
        anjunaBeach.addActivity(paragliding);
        anjunaBeach.addActivity(treaking);


        //testing, as we're using set data structure for activities at destination, so trying to insert duplicate element and it won't insert as we have override the hashcode & equals method
        Activity waterSportDup = new Activity("Water Sport", "Get ready for the fun and adventure that awaits you at Baga Beach", 5000.);
        anjunaBeach.addActivity(waterSportDup);


//        for (Activity activity : anjunaBeach.getActivities()) {
//            System.out.println(activity);
//        }

        //trying to modify the set of activities of destination from outside
//        Activity hello = new Activity("hello", "", 1000);
//        anjunaBeach.getActivities().add(hello);
        //it'll not modify the set of activities, and it will raise an exception, because we're returning unmodifiableSet and it will not modify the collection after creation.

        Activity cycling = new Activity("Cycling", "Get ready for the fun and adventure that awaits you at Baga Beach", 5000.);
        Activity scubaDiving = new Activity("Scuba Diving", "Get ready for the fun and adventure that awaits you at Baga Beach", 1000.);
        Activity fishing = new Activity("Fishing", "Get ready for the fun and adventure that awaits you at Baga Beach", 2000.);

        Destination bagaBeach = new Destination("Baga Beach");

        //testing whether activity is available at one destination only
        //bagaBeach.addActivity(waterSport);

        bagaBeach.addActivity(cycling);
        bagaBeach.addActivity(scubaDiving);
        bagaBeach.addActivity(fishing);

        goa.addDestination(anjunaBeach);
        goa.addDestination(bagaBeach);

        Set<Passenger> passengers = Set.of(
                new StandardPassenger("Tara Maheswari", 12345, 10000.0),
                new StandardPassenger("Heer Kaul", 12456, 5000.0),
                new GoldPassenger("Ved Sahani", 12457, 25000.),
                new GoldPassenger("Jordan ford", 12349, 30000.0),
                new GoldPassenger("Jerry Patel", 12340, 40000.0),
                new StandardPassenger("Gaurav Katariya", 12342, 12000.0),
                new StandardPassenger("Tushar Gajwani", 12341, 9000.0),
                new StandardPassenger("Rashmi Sajnani", 12346, 80000.0),
                new PremiumPassenger("Ayushi Wallecha", 34566),
                new PremiumPassenger("Vicky Vaswani", 23445)
        );


        goa.detailsTravelPackage();

        System.out.println("Passenger List:");
        //goa.addPassengerList(passengers);


        Passenger newPassenger = new StandardPassenger("New", 12349, 80000.0);
        Passenger newPassenger2 = new StandardPassenger("New2", 12350, 80000.0);
        goa.addPassenger(newPassenger);
        goa.addPassenger(newPassenger2);

        newPassenger.signForActivity(anjunaBeach, waterSport);
        newPassenger.signForActivity(anjunaBeach, paragliding);


        //newPassenger.signForActivity(bagaBeach, cycling);


//        newPassenger.signForActivity(bagaBeach, waterSport);

    /*    goa.availableSpacesAtActivities();
//        for (Passenger passenger : passengers) {
//            //  passenger.signForActivity(anjunaBeach, );
//        }

        goa.details(newPassenger);

        goa.passengerList();
    */



    }
}
