package src.design.destination;

import java.util.Objects;

/**
 * Activity class describes about the activity at the particular destination, and it belongs to one destination only.
 */
public class Activity {

    private String name, description;
    private double cost;
    private int capacity, size;
    private boolean belongsTo;

    /**
     * Constructs the activity with default capacity 10
     */
    public Activity(String name, String description, double cost) {
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.size = 0;
        this.capacity = 10;
        this.belongsTo = false;
    }

    /**
     * Constructs the activity with initial capacity, and provided args
     * @param name, description, cost, capacity
     * @throws IllegalArgumentException if capacity provided is less than 0
     */
    public Activity(String name, String description, double cost, int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException("Illegal capacity");
        }
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.capacity = capacity;
        this.size = 0;
        this.belongsTo = false;
    }

    /**
     * getters of the property
     * @return
     */
    public int getCapacity() {
        return this.capacity;
    }
    public String getName() {
        return this.name;
    }
    public double getCost() {
        return this.cost;
    }
    public String getDescription() {
        return this.description;
    }

    /**
     * get the occupied size of the activity
     */
    public int getOccupiedSize() {
        return this.size;
    }


    /**
     * checks whether the activity has capacity or not.
     * @return
     */
    public boolean hasCapacity() {
        return size < this.capacity;
    }

    /**
     * this method is used to check whether the activity belongs to other destination or not
     * @return
     */
    public boolean belongsToDestination() {
        if (!this.belongsTo) {
            this.belongsTo = true;
            return false;
        }
        return true;
    }

    /**
     * increase the size of activity when the passenger signs for the activity
     */
    public void increaseSize() {
        ++this.size;
    }

    @Override
    public boolean equals(Object activity) {
        if (this == activity) {
            return true;
        }
        if (activity instanceof Activity) {
            return this.getName().equals(((Activity) activity).getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getName());
    }

    @Override
    public String toString()
    {
        return "[" + this.getName() + " activity" + "\n"
                + "description: " + this.getDescription() + "\n"
                + "cost: " + this.getCost() + "\n"
                + "capacity: " + this.getCapacity() + "\n"
                + "]";
    }
}
