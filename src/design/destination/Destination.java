package src.design.destination;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * This class defines about the destination. Destination has a name && set of activities associated with that activities.
 * Set data structure is used to maintain the activities in it, so that their is no duplicate activity object.
 *
 * Destination -> has a relationship with -> Activity
 */
public class Destination {

    private String name;
    private Set<Activity> activities;

    public Destination(String name) {
        this.name = name;
        this.activities = new HashSet<>();
    }

    /**
     * this method will add the activity in Destination, if the activity already belongs to some other destination
     * then it will throw an exception.
     * @param activity
     * @return boolean whether activitiy is pushed in set of activities.
     */
    public boolean addActivity(Activity activity) {
        if (activity.belongsToDestination()) {
            throw new RuntimeException(activity.getName() + "  already belongs to some other destination");
        }
        return this.activities.add(activity);
    }

    /**
     * method will return the set of activities associated with this destination
     * public Set<Activity> getActivities() { return this.activities; }
     * @return Set<Activity>
     **/
    public Set<Activity> getActivities() {
        return Collections.unmodifiableSet(this.activities);
    }

    /**
     * get the name of destination
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * This method is used to check whether this destination has this activity associated with it or not.
     * @param activity
     * @return bool
     */
    public boolean has(Activity activity) {
        return this.activities.contains(activity);
    }
}