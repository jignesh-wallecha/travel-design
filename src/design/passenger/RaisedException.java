package src.design.passenger;

import src.design.destination.Activity;
import src.design.destination.Destination;

/**
 * This interface is used to raise unchecked exceptions when passenger signs for the activity and some validation is done
 * before the passenger can actually signs/participate for the activity.
 */
public interface RaisedException {

    default void throwIfInSufficientBalance(Passenger passenger, Activity activity) {
        if (activity.getCost() > passenger.getBalance()) {
            throw new RuntimeException("You don't have more balanace to sign for this activity");
        }
    }

    default void throwIfDestinationDoesNotHaveActivity(Destination destination, Activity activity) {
        if (! destination.has(activity)) {
            throw new RuntimeException(destination.getName() + " does not have this " + activity.getName() + " activity");
        }
    }

    default void throwIfNoMoreSpaces(Activity activity) {
        if (! activity.hasCapacity()) {
            throw new RuntimeException(activity.getName() + " does not have any capacity more");
        }
    }
}
