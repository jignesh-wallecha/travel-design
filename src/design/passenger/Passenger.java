package src.design.passenger;

import java.util.*;

import src.design.destination.Activity;
import src.design.destination.Destination;

/**
 * This is the absract class, this class describes the about the passenger.
 *
 */
public abstract class Passenger implements RaisedException {

    protected final String name;
    protected final int passengerNum;
    protected double balance;
    protected Map<Destination, HashSet<Activity>> activities;
    protected Map<Activity, Double> priceOfActivity;

    protected Passenger(String name, int passengerNum, double balance) {
        this.passengerNum = passengerNum;
        this.name = name;
        this.balance = balance;
        this.activities = new HashMap<>();
        this.priceOfActivity = new HashMap<>();
    }

    /**
     * getters of the properties
     * @return
     */
    public String getName() {
        return this.name;
    }
    public int getPassengerNum() {
        return this.passengerNum;
    }
    public double getBalance() { return this.balance; }

    /**
     * get the activities of passenger
     * @return Map<Destination, HashSet<Activity>>
     */
    public Map<Destination, HashSet<Activity>> getActivities() {
        return Collections.unmodifiableMap(activities);
    }

    /**
     * get the activities of the given destination
     * @param $destination
     * @return
     */
    public HashSet<Activity> getActivities(Destination $destination) {
        return this.activities.get($destination);
    }

    /**
     * Get the price paid by passenger for this activity
     * if the passenger didn't particpated in activity, then their will be no record so return 0, in this case
     * @param activity
     * @return
     */
    public double getPaidPrice(Activity activity) {
        if (! priceOfActivity.containsKey(activity)) {
           return 0;
        }
        return this.priceOfActivity.get(activity);
    }


    /**
     * Passenger will sign for an activity at particular destination
     * @param destination
     * @param activity
     */
    abstract public void signForActivity(Destination destination, Activity activity);

    abstract public String getPackageName();

    @Override
    public boolean equals(Object passenger) {
        return this == passenger || 
               passenger instanceOf Passenger && 
               this.getPassengerNum() == ((Passenger) passenger).getPassengerNum();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getPassengerNum());
    }

    @Override
    public String toString() {
        return "[Name:" + this.name + "\n" +
                " PassengerNumber:" + this.passengerNum + " \n" +
                " Balance: " + this.balance + "\n" +
                " Package: " + this.getPackageName() + "\n" +
                "]";
    }
}
