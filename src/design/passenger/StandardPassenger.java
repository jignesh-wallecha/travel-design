package src.design.passenger;

import src.design.destination.Activity;
import src.design.destination.Destination;

import java.util.HashSet;

/**
 * The Standard Passenger is the normal passenger, signs up for an activity and the cost is deducted from the balance.
 * They cannot sign up for an activity if they do not have sufficient balance.
 */
public class StandardPassenger extends Passenger {

    private static final String STANDARD_PACKAGE = "STANDARD_PACKAGE";

    public StandardPassenger(String name, int passengerNum, double balance) {
        super(name, passengerNum, balance);
    }

    @Override
    public synchronized void signForActivity(Destination destination, Activity activity) {

        //check whether this destination has this activity || check for capacity of activity ||
        // check the sufficient balance
        this.throwIfDestinationDoesNotHaveActivity(destination, activity);

        this.throwIfNoMoreSpaces(activity);

        this.throwIfInSufficientBalance(this, activity);

        //sign for an activity
        HashSet<Activity> setActivities;
        if (activities.containsKey(destination)) {
            setActivities = activities.get(destination);
        } else {
            setActivities = new HashSet<>();
        }

        setActivities.add(activity);
        activities.put(destination, setActivities);

        // deduct the cost from balance
        this.balance -= activity.getCost();

        // add the record, for price paid for the activity
        priceOfActivity.put(activity, activity.getCost());

        //increase the size of activity
        activity.increaseSize();
    }

    @Override
    public String getPackageName() {
        return STANDARD_PACKAGE;
    }


}
