package src.design.passenger;

import src.design.destination.Activity;
import src.design.destination.Destination;

import java.util.HashSet;

/**
 * Premium Passenger can sign for any activity for free. This passenger does not have any balance.
 */
public class PremiumPassenger extends Passenger {

    private static final String PREMIUM_PACKAGE = "PREMIUM_PACKAGE";

    public PremiumPassenger(String name, int passengerNum) {
        super(name, passengerNum, 0);
    }

    @Override
    public synchronized void signForActivity(Destination destination, Activity activity) {

        //check whether this destination has this activity ||
        this.throwIfDestinationDoesNotHaveActivity(destination, activity);

       // check for capacity of activity
        this.throwIfNoMoreSpaces(activity);

        //sign for the activity
        HashSet<Activity> setActivities;
        if (activities.containsKey(destination)) {
            setActivities = activities.get(destination);
        } else {
            setActivities = new HashSet<>();
        }

        setActivities.add(activity);
        activities.put(destination, setActivities);

        //increase the size of the activity
        activity.increaseSize();
    }

    @Override
    public String getPackageName() {
        return PREMIUM_PACKAGE;
    }
}
