package src.design.passenger;

import src.design.destination.Activity;
import src.design.destination.Destination;

import java.util.HashSet;

/**
 * GoldPassenger class is a type of Passenger, it extends the Passenger object.
 * Each time gold passenger signs for the activity at some destinations he/she will get the 10% discount on the cost
 * of the activity
 */
public class GoldPassenger extends Passenger {

    private static final String GOLD_PACKAGE = "GOLD_PACKAGE";

    public GoldPassenger(String name, int passengerNum, double balance) {
        super(name, passengerNum, balance);
    }

    @Override
    public synchronized void signForActivity(Destination destination, Activity activity) {

        //check whether this destination has this activity || check for capacity of activity ||
        // check the sufficient balance
        this.throwIfDestinationDoesNotHaveActivity(destination, activity);

        this.throwIfNoMoreSpaces(activity);

        this.throwIfInSufficientBalance(this, activity);


        HashSet<Activity> setActivities;
        if (activities.containsKey(destination)) {
            setActivities = activities.get(destination);
        } else {
            setActivities = new HashSet<>();
        }

        //sign for an activity
        setActivities.add(activity);
        activities.put(destination, setActivities);

        this.balance -= getDiscountedAmount(activity);

        // add the record, for price paid for the activity
        priceOfActivity.put(activity, getDiscountedAmount(activity));

        //increase the size of activity
        activity.increaseSize();
    }

    /**
     * this method's discount is static, which is 10%, so it will give discounted amount of given activity
     * @param activity
     * @return discountedAmount
     */
    private double getDiscountedAmount(Activity activity) {
        double $activityCost = activity.getCost();
        return $activityCost - ($activityCost * 10 / 100);
    }


    @Override
    public String getPackageName() {
        return GOLD_PACKAGE;
    }
}
