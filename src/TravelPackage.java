package src;

import src.design.destination.Activity;
import src.design.destination.Destination;
import src.design.passenger.Passenger;
import src.design.passenger.PremiumPassenger;

import java.util.*;

/**
 * Travel package has a name, List of iteneraries & passengers
 */
public class TravelPackage {

    private String name;
    private int passengerCapacity;
    private List<Destination> itineraries;
    private Set<Passenger> passengers;

    public TravelPackage(String name) {
        this.name = name;
        this.passengerCapacity = 20;
        this.itineraries = new ArrayList<>();
        this.passengers = new HashSet<>(passengerCapacity);
    }

    public TravelPackage(String name, int passengerCapacity) {
        this.name = name;
        this.passengerCapacity = passengerCapacity;
        this.itineraries = new ArrayList<>();
        this.passengers = new HashSet<>(passengerCapacity);
    }

    /**
     * this method adds the passenger to the list.
     * @param passenger
     */
    public void addPassenger(Passenger passenger) {
        passengers.add(passenger);
    }

    /**
     * this method is used to add set of passengers all at once in tarvel package
     * @param Set<Passenger> passengers
     */
    public void addPassengerList(Set<Passenger> passengers) {
        this.passengers = passengers;
    }

    /**
     * this method adds the destination to the itenerary list.
     * @param destination
     */
    public void addDestination(Destination destination) {
        itineraries.add(destination);
    }
    /**
     * this method adds the collection of itenerary to the iteneraries list.
     * @param List<Destination>
     */
    public void addDestinationList(List<Destination> destinations) { this.itineraries = destinations; }

    public String getPackageName() {
        return this.name;
    }

    public Set<Passenger> getPassengers() {
        return Collections.unmodifiableSet(passengers);
    }

    /**
     * get the destinations of the travel package
     * @return
     */
    public List<Destination> getDestinations() {
        return Collections.unmodifiableList(this.itineraries);
    }

    /**
     * prints the detail of Travel Package
     */
    public void detailsTravelPackage() {
          System.out.println("Package Name: " + this.getPackageName());
          itineraries.stream()
                    .peek(destination -> {
                        System.out.println("Destination: " + destination.getName() + "\nActivities: ");
                    })
                    .map(Destination::getActivities)
                    .forEach(System.out::println);
    }

    /**
     * print the passenger list of the travel package
     */
    public void passengerList() {
        System.out.println("Passenger Capacity: " + passengerCapacity);
        passengers.stream().forEach(System.out::println);
    }


    /**
     * print the details of an individual passenger
     * @param passenger
     */
    public void details(Passenger passenger) {
        if (! passengers.contains(passenger)){
            throw new RuntimeException("Passenger does not contains");
        }
        System.out.println("Name: " + passenger.getName() + " PassengerNum: " + passenger.getPassengerNum());

        if (! (passenger instanceof PremiumPassenger)) {
            System.out.println("Balance: " + passenger.getBalance());
        }

        System.out.println("Activities:");
        for (Map.Entry<Destination, HashSet<Activity>> entry : passenger.getActivities().entrySet()) {
            System.out.println("\nDestination: " + entry.getKey().getName() + "");
            for (Activity activity : entry.getValue()) {
                System.out.println("Activity: " + activity.getName() + " price paid: " + passenger.getPaidPrice(activity));
            }
            System.out.println();
        }
    }

    /**
     * this method will print the details of all activities that have spaces, including how many spaces are available
     */
    public void availableSpacesAtActivities() {
        itineraries.stream()
                .map(Destination::getActivities)
                .forEach(activities -> {
                    activities.stream()
                            .filter(Activity::hasCapacity)
                            .forEach(activity ->  {
                                int availableSpaces = activity.getCapacity() - activity.getOccupiedSize();
                                System.out.println(activity.getName() + " has spaces " + availableSpaces + "available");
                            });
                });

    }


}